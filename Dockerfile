FROM node:22-alpine AS salesapp

WORKDIR /src
COPY package*.json ./
RUN npm ci
COPY . .
EXPOSE 3000
RUN npm run start:dev

#FROM node:22-alpine

#WORKDIR /src
#ENV TZ=Europe/Istanbul
#COPY --from=BASEIMAGE /src/dist /src/dist
#COPY --from=BASEIMAGE /src/node_modules /src/node_modules

#CMD ["node", "dist/main.js"]