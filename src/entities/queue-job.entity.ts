import { Entity, BaseEntity, Column, Index, PrimaryColumn, ObjectIdColumn, ObjectId } from "typeorm";

@Entity()
export class QueueJob extends BaseEntity {
  
    @ObjectIdColumn()
    id: ObjectId

    @Column({ unique: true })
    message_id: string;

    @Column('json')
    message: any;

    @Column('json')
    entity: any;

    @Column()
    queue: string;

    @Column()
    job_type: string;

    @Column({default:0})
    @Index()
    status: number;

    @Column({default:0})
    counter: number;

    @Column('json', { nullable: true })
    error: any;

    @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
    created_at: Date;
  
    @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
    updated_at: Date;

  }