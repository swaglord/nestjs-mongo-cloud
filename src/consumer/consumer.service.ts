import { Message } from "@aws-sdk/client-sqs";
import { Injectable, Logger, InternalServerErrorException } from "@nestjs/common";
import { SqsMessageHandler } from "@ssut/nestjs-sqs";
import { MessageBody } from "aws-sdk/clients/pinpoint";
import { QueueJobService } from "src/producer/queue-job.service";

@Injectable()
export class ConsumerService {
  constructor(
    private readonly queueJobService: QueueJobService,
    //private readonly jobHandlerFactory: JobHandlerFactory,
  ) {}

  @SqsMessageHandler(/** name: */ '' /*config.sqs.queue_name, /** batch: */ /*false*/)
  async handleMessage(message: Message) {
    const msgBody: MessageBody = JSON.parse(message.Body) as MessageBody;
    console.log('Consumer  Start ....:', msgBody);

    /*if (!JOB_TYPES.includes(msgBody.MessageAttributes.job.value)) {
      Logger.error('Invalid job type ' + msgBody.MessageAttributes.job.value);
      throw new InternalServerErrorException(
        'Invalid job type ' + msgBody.MessageAttributes.job.value,
      );
    }*/

    try {
      //Todo
      // handle the message here
   
    } catch (error) {
      console.log('consumer error', JSON.stringify(error));
      //keep the message in sqs
      Logger.error(error.message);
      throw new InternalServerErrorException(error);
   
    }
  }
}