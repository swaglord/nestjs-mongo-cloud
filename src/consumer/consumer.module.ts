import { SQSClient } from "@aws-sdk/client-sqs";
import { Module } from "@nestjs/common";
import { ConfigModule } from "@nestjs/config";
import { SqsModule } from "@ssut/nestjs-sqs";
import { ProducerModule } from "src/producer/producer.module";
import { ConsumerService } from "./consumer.service";


@Module({
  imports: [
    ProducerModule,
    SqsModule.registerAsync({
      imports: [ConfigModule], // Import the ConfigModule to use the ConfigService
      useFactory: async () => {
        const accessKeyId = process.env.SQS_ACCESSKEYID//configService.get<string>('sqs.accessKeyId');
        const secretAccessKey = process.env.SQS_SECRETACCESSKEY//configService.get<string>(
         // 'sqs.secretAccessKey',
        //);

        // Retrieve the required configuration values using ConfigService
        return {
          consumers: [
            {
              name:'',// configService.get<string>('sqs.queue_name'), // name of the queue
              queueUrl: process.env.SQS_URL,//configService.get<string>('sqs.url'), // url of the queue
              region: process.env.SQS_REGION,//configService.get<string>('sqs.region'), // using the same region for the producer
              batchSize: 10, // number of messages to receive at once
              // visibilityTimeout:10,
              // waitTimeSeconds:300,
              terminateGracefully: true, // gracefully shutdown when SIGINT/SIGTERM is received
              sqs: new SQSClient({
                region: process.env.SQS_REGION,//configService.get<string>('sqs.region'),
                credentials: {
                  accessKeyId: accessKeyId,
                  secretAccessKey: secretAccessKey,
                },
              }),
            },
          ],
          producers: [],
        };
      },
      inject: [],
    }),
  ],
  controllers: [],
  providers: [
    ConsumerService
  ],
  exports: [ConsumerService],
})
export class ConsumerModule {}