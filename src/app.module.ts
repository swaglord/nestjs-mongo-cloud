import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CatsService } from './cats/cats.service';
import { CatsController } from './cats/cats.controller';
import { LoggerMiddleware } from './logger.middleware';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { join } from 'path';
import { ConfigModule } from '@nestjs/config';
import { QueueJobController } from './producer/queue-job.controller';
import { ProducerService } from './producer/producer.service';
import { QueueJobService } from './producer/queue-job.service';
import { ProducerModule } from './producer/producer.module';
import { ConsumerModule } from './consumer/consumer.module';
import { ConsumerService } from './consumer/consumer.service';
import { SqsModule } from '@ssut/nestjs-sqs/dist/sqs.module';
import { ConfigService } from './config.service';
import { CustomConfigModule } from './config.module';

@Module({
  imports: [AuthModule, 
    ConfigModule.forRoot(),
    TypeOrmModule.forRoot({
      type: "mongodb",
      url: process.env.MONGO_URL || "mongodb://localhost:27017/nest",
      "useNewUrlParser": true,
      "synchronize": true,
      "logging": true,
      entities: [join(__dirname, '**', '*.entity.{ts,js}')]
    }),
  UsersModule,
  ProducerModule,
  ConsumerModule],
  controllers: [AppController, CatsController, QueueJobController],
  providers: [AppService, CatsService, ProducerService, QueueJobService, ConsumerService, ConfigService],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(LoggerMiddleware)
      .forRoutes('cats');
  }
}

/* TEST COMMENT */
