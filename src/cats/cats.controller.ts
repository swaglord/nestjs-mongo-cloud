import { Controller, Get } from '@nestjs/common';
import { CatsService } from './cats.service';
import { Public } from '../auth/public-strategy';

@Controller('cats')
export class CatsController {
    constructor(private catsService: CatsService) {}

    @Get()
    @Public()
    getHello(): string {
      return this.catsService.getHello();
    }
}
