import { Injectable, Logger } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { BaseService } from "src/base.service";
import { QueueJob } from "src/entities/queue-job.entity";
import { MongoRepository } from "typeorm";

@Injectable()
export class QueueJobService extends BaseService<QueueJob> {
  private readonly logger = new Logger(QueueJobService.name);
  constructor(
    @InjectRepository(QueueJob)
    protected readonly repository: MongoRepository<QueueJob>,
  ) {
    super(repository);
  }
}