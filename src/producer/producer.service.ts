import { Injectable, BadRequestException, InternalServerErrorException } from "@nestjs/common";
import * as AWS from "aws-sdk";
import { defer, switchMap, from, tap, catchError } from "rxjs";
import { QueueJob } from "src/entities/queue-job.entity";
import { uuid } from "uuidv4";
import { QueueJobService } from "./queue-job.service";
import { ConfigService } from "src/config.service";

export interface SQSMessage {
    QueueUrl: string;
    MessageBody: string;
    MessageGroupId?: string;
    MessageDeduplicationId?: string;
    DelaySeconds?: number;
}

export interface Job {
  DataType: string;
  value: string;
}
export interface MessageAttributes {
  job: Job;
}

export interface MessageBody {
  messageId:string;
  message: any;
  date: string;
  MessageAttributes: MessageAttributes;
}

@Injectable()
export class ProducerService {
  constructor(
    private readonly configService: ConfigService,
    private readonly queueJobService: QueueJobService,
  ) {}

  send(message: any, jobType: string, messageGroupId: string = 'general') {
    /*if (!JOB_TYPES.includes(jobType)) {
      throw new BadRequestException('Invalid job type');
    }*/
    const region = this.configService.get('sqs.region');
    const accessKeyId = this.configService.get('sqs.accessKeyId');
    const secretAccessKey = this.configService.get(
      'sqs.secretAccessKey',
    );

    const isFifo: boolean = JSON.parse(this.configService.get('sqs.isFifo'));

    const messageId = uuid() ?? "";
    let sqsMessage: SQSMessage = {
      QueueUrl: this.configService.get('sqs.url'),
      MessageBody: JSON.stringify({
        messageId,
        message,
        MessageAttributes: {
          job: {
            DataType: 'string',
            value: jobType,
          },
        },
      } as MessageBody),
    };
    
    if (isFifo === true) {
      sqsMessage = {
        ...sqsMessage,
        MessageGroupId: messageGroupId,
        MessageDeduplicationId: messageId,
      };
    }

    const sqs = new AWS.SQS({
      region,
      accessKeyId,
      secretAccessKey,
    });
    
    const input: Partial<QueueJob> = {
      message_id: messageId,
      message: sqsMessage,
      entity: message,
      job_type: jobType,
      queue: this.configService.get('sqs.queue_name'),
    };

    return defer(() => this.queueJobService.save(input)).pipe(
      switchMap(() => {
        return from(sqs.sendMessage(sqsMessage).promise()).pipe(
          tap(() => {
            console.log('Message sent');
            return true;
          }),
          catchError((error) => {
            console.log('error:', error);
            throw new InternalServerErrorException(error);
          }),
        );
      }),
    );
  }
}
