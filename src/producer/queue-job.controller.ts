import { Body, Controller, Post, HttpCode, HttpStatus } from "@nestjs/common";
import { ApiOperation, ApiResponse, ApiTags } from "@nestjs/swagger";
import { BaseUser } from "../dto/user/base-user.dto";
import { Public } from "../auth/public-strategy";
import { ProducerService } from "./producer.service";

@Controller("queue-job")
@ApiTags("queue-job")
export class QueueJobController {
  constructor(private producerService: ProducerService) {} 
  
  @Public()
  @HttpCode(HttpStatus.OK)
  @Post()
  @ApiOperation({ summary: "Create Job in AWS SQS" })
  @ApiResponse({
    status: 200,
    description: "Job created successfully",
    type: [BaseUser],
  })
  create(@Body() queueJobDto: Record<string, any>) {
    console.log(queueJobDto)
    return this.producerService.send(queueJobDto, 'queue-job', 'queue-job');
  }  
  
}