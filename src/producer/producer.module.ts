import { Module } from '@nestjs/common';
import * as AWS from 'aws-sdk';
import { ProducerService } from './producer.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { QueueJob } from '../entities/queue-job.entity';
import { QueueJobService } from './queue-job.service';
import { ConfigService } from '../config.service'
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [TypeOrmModule.forFeature([QueueJob]), ConfigModule.forRoot()],
  providers: [
    {
      provide: AWS.SQS,
      useFactory: (configService: ConfigService) => {
        const region = configService.get('sqs.region');
        const accessKeyId = configService.get('sqs.accessKeyId');
        const secretAccessKey = configService.get(
          'sqs.secretAccessKey',
        );
        const sqsConfig = {
          region,
          accessKeyId,
          secretAccessKey,
        };
        return new AWS.SQS(sqsConfig);
      },
      inject: [ConfigService],
    },
    ProducerService,
    QueueJobService,
    ConfigService
  ],
  exports: [    ConfigService, AWS.SQS,
    ProducerService,
    QueueJobService,
    TypeOrmModule.forFeature([QueueJob]),],
})
export class ProducerModule {}