// BaseService.ts

import {
    DeepPartial,
    FindManyOptions,
    FindOneOptions,
    Repository,
    QueryRunner,
    UpdateResult,
    SelectQueryBuilder,
    FindOptionsWhere,
  } from 'typeorm';
  import { Observable, defer, from, map, switchMap } from 'rxjs';
  import { QueryDeepPartialEntity } from 'typeorm/query-builder/QueryPartialEntity';
  
  export abstract class BaseService<T> {
    constructor(protected readonly repository: Repository<T>) {}

    save(entity: DeepPartial<T>): Observable<T> {
      return defer(() => this.repository.save(entity));
    }
  
    update(id: any, entity: QueryDeepPartialEntity<T>): Observable<UpdateResult> {
      return defer(() => this.repository.update(id, entity));
    }
  
    increment(
      entity: FindOptionsWhere<T>,
      field: string,
      incremental: number = 1,
    ): Observable<any> {
      return defer(() => this.repository.increment(entity, field, incremental));
    }
   
  }